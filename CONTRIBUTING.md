# Contributing to the Calli File Server

First of all, thank you for taking the time to contribute!

The following is a set of guidelines on how to contribute to the Iterator example.

## Code of Conduct

This project and everyone participating in it are expected to uphold the [Rust code of conduct](https://www.rust-lang.org/policies/code-of-conduct), please report unacceptable behaviour to one of the maintainers.

## Environment details

Tests can be run using `cargo test`.

Please format code using `cargo +nightly fmt` if you have made changes or added something and use `cargo clippy` to check for possible improvements.

## Submit Changes

Changes should be submitted through use of a merge request and should conform to the following requirements:

 - The merge request description should describe what the change is for
 - The merge request description should link the issue that the change resolves
 - All tests have to pass
 - If the change adds functionality, new tests should be created that cover the new functionality

## Issues

If there is anything you want to add or change or see added or changed, please take a look at our [issues](https://gitlab.com/CalliEv/file-server/-/issues). If one matching what you want to do doesn't yet exists, please create it.
Issues are tagged by the type of issue that they are, the following are some of these tags:

### - bug

Please submit bug reports by creating an issue and giving it this bug tag. Bug reports should include a minimal example on how to reproduce the behaviour.

### - enhancement

Please submit and enhancement request by creating an issue and giving it this enhancement tag. Enhancement requests should include an usage example if applicable.

### - good-first-issue

These are issues that should be simple to resolve and are also good to do for programmers who do not yet have a lot of experience.
