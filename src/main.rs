use std::{
    env,
    net::{
        IpAddr,
        SocketAddr,
    },
};

use file_server::{
    endpoints::*,
    make_router_service,
    routes,
    ConfigFile,
};
use hyper::Server;

async fn shutdown_signal() {
    // Wait for the CTRL+C signal
    tokio::signal::ctrl_c()
        .await
        .expect("failed to install CTRL+C signal handler");
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let port: u16 = env::var("CDN_PORT")
        .expect("no cdn port provided")
        .parse()
        .expect("not a valid port");
    let host: IpAddr = env::var("CDN_HOST")
        .expect("no cdn host provided")
        .parse()
        .expect("not a valid ip address");

    let cfg = ConfigFile::new()
        .await
        .unwrap();

    let addr = SocketAddr::from((host, port));

    let endpoints = routes![
        authenticate,
        create_user,
        update_user,
        delete_user,
        get_file_endpoint,
        post_file_endpoint,
        delete_file_endpoint,
        post_folder_endpoint,
        delete_folder_endpoint,
        make_public_endpoint,
        hide_public_endpoint
    ];

    let server = Server::bind(&addr).serve(make_router_service(cfg, endpoints));
    let graceful = server.with_graceful_shutdown(shutdown_signal());

    graceful
        .await
        .unwrap();
}
