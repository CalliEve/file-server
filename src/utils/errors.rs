use std::io::ErrorKind;

use hyper::{
    Body,
    Response,
    StatusCode,
};

/// The error enum used in this crate to turn errors into.
pub enum Error {
    Hyper(hyper::Error),
    Io(std::io::Error),
    Json(serde_json::Error),
    InvalidArgument(String),
    AlreadyExists,
    NotFound,
    Unauthorized,
    NoToken,
    NotAllowed,
}

impl Error {
    /// Writes the error to a [`Response`].
    pub fn make_resp(&self, resp: &mut Response<Body>) {
        *resp.body_mut() = format!(r#"{{"message":"{}"}}"#, self).into();
        *resp.status_mut() = match self {
            Self::NotFound => StatusCode::NOT_FOUND,
            Self::InvalidArgument(_) | Self::AlreadyExists => StatusCode::BAD_REQUEST,
            Self::Unauthorized | Self::NoToken => StatusCode::UNAUTHORIZED,
            Self::NotAllowed => StatusCode::FORBIDDEN,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

pub type Result<T> = std::result::Result<T, Error>;

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        match e.kind() {
            ErrorKind::NotFound => Self::NotFound,
            ErrorKind::AlreadyExists => Self::AlreadyExists,
            _ => Self::Io(e),
        }
    }
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Self::Json(e)
    }
}

impl From<hyper::Error> for Error {
    fn from(e: hyper::Error) -> Self {
        Self::Hyper(e)
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Hyper(e) => std::fmt::Display::fmt(&e, f),
            Error::Io(e) => std::fmt::Display::fmt(&e, f),
            Error::Json(e) => std::fmt::Display::fmt(&e, f),
            Error::InvalidArgument(e) => {
                f.write_str(format!("Invalid argument provided: {}", e).as_str())
            },
            Error::AlreadyExists => f.write_str("Target already exists"),
            Error::NotFound => f.write_str("Target does not exist"),
            Error::Unauthorized => f.write_str("Invalid username or password"),
            Error::NoToken => f.write_str("No token provided"),
            Error::NotAllowed => f.write_str("Not allowed access or invalid token"),
        }
    }
}

impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Hyper(e) => std::fmt::Debug::fmt(&e, f),
            Error::Io(e) => std::fmt::Debug::fmt(&e, f),
            Error::Json(e) => std::fmt::Debug::fmt(&e, f),
            Error::InvalidArgument(e) => f
                .debug_tuple("InvalidArgument")
                .field(&e)
                .finish(),
            Error::AlreadyExists => f.write_str("AlreadyExists"),
            Error::NotFound => f.write_str("NotFound"),
            Error::Unauthorized => f.write_str("Unauthorized"),
            Error::NoToken => f.write_str("NoToken"),
            Error::NotAllowed => f.write_str("NotAllowed"),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        Some(match self {
            Error::Hyper(e) => e,
            Error::Io(e) => e,
            Error::Json(e) => e,
            _ => return None,
        })
    }
}
