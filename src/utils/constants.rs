use once_cell::sync::Lazy;

/// The root directory for storing and reading the files for the file server
/// from.
pub static FILE_DIR: Lazy<String> =
    Lazy::new(|| std::env::var("CDN_FILE_DIR").unwrap_or_else(|_| "./file-dir".to_string()));
