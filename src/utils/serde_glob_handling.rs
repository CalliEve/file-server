use glob::Pattern;
use serde::{
    self,
    de::Error,
    de::Unexpected,
    Deserialize,
    Deserializer,
    Serializer,
};

/// Serializes a [`Pattern`] using serde
pub fn serialize<S>(pat: &Pattern, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    serializer.serialize_str(pat.as_str())
}

/// Deserializes a [`Pattern`] from serde.
pub fn deserialize<'de, D>(deserializer: D) -> Result<Pattern, D::Error>
where
    D: Deserializer<'de>,
{
    Pattern::new(&String::deserialize(deserializer)?)
        .map_err(|e| D::Error::invalid_value(Unexpected::Other(&e.to_string()), &"glob pattern"))
}

pub mod with_vec {
    use glob::Pattern;
    use serde::{
        self,
        ser::SerializeSeq,
        Deserialize,
        Deserializer,
        Serialize,
        Serializer,
    };

    /// A wrapper around a [`Pattern`] for serializing and deserializing using
    /// serde.
    struct WrapPattern(Pattern);

    impl Serialize for WrapPattern {
        fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            super::serialize(&self.0, serializer)
        }
    }

    impl<'de> Deserialize<'de> for WrapPattern {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: Deserializer<'de>,
        {
            Ok(Self(super::deserialize(deserializer)?))
        }
    }

    /// Serializes a slice of [`Pattern`]s using serde.
    pub fn serialize<S>(pats: &[Pattern], serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(pats.len()))?;

        for pat in pats {
            seq.serialize_element(&WrapPattern(pat.clone()))?;
        }

        seq.end()
    }

    /// Deserializes to a vec of [`Pattern`].
    pub fn deserialize<'de, D>(deserializer: D) -> Result<Vec<Pattern>, D::Error>
    where
        D: Deserializer<'de>,
    {
        Ok(Vec::<WrapPattern>::deserialize(deserializer)?
            .into_iter()
            .map(|w| w.0)
            .collect())
    }

    #[cfg(test)]
    mod tests {
        use serde::{
            Deserialize,
            Serialize,
        };

        use super::Pattern;

        #[derive(Deserialize, Serialize, PartialEq, Debug)]
        struct Patterns {
            #[serde(with = "super")]
            inner: Vec<Pattern>,
        }

        #[test]
        fn test_vec_patterns_serde() {
            let patterns = Patterns {
                inner: vec![
                    Pattern::new("*.*").unwrap(),
                    Pattern::new("/x/**/f.*").unwrap(),
                ],
            };

            let serialized = serde_json::to_string(&patterns).unwrap();

            assert_eq!(r#"{"inner":["*.*","/x/**/f.*"]}"#, &serialized);

            let deserialized: Patterns = serde_json::from_str(&serialized).unwrap();

            assert_eq!(patterns, deserialized);
        }
    }
}

#[cfg(test)]
mod tests {
    use serde::{
        Deserialize,
        Serialize,
    };

    use super::Pattern;

    #[derive(Deserialize, Serialize, PartialEq, Debug)]
    struct PatternHolder {
        #[serde(with = "super")]
        inner: Pattern,
    }

    #[test]
    fn test_vec_patterns_serde() {
        let pattern = PatternHolder {
            inner: Pattern::new("/x/**/f.*").unwrap(),
        };

        let serialized = serde_json::to_string(&pattern).unwrap();

        assert_eq!(r#"{"inner":"/x/**/f.*"}"#, &serialized);

        let deserialized: PatternHolder = serde_json::from_str(&serialized).unwrap();

        assert_eq!(pattern, deserialized);
    }
}
