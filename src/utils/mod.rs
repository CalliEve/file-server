pub mod constants;
pub mod errors;
pub mod form_data;
pub mod serde_glob_handling;

pub use errors::Error;
use futures::stream::StreamExt;
use serde::de::DeserializeOwned;

/// Converts a [`Body`] to a [`Vec`] of [`u8`].
///
/// [`Body`]: hyper::Body
pub async fn body_to_vec(body: &mut hyper::Body) -> Result<Vec<u8>, errors::Error> {
    body.by_ref()
        .fold(Ok(Vec::<u8>::new()), |acc, b_res| async move {
            match acc {
                Err(_) => acc,
                Ok(mut buf) => match b_res {
                    Ok(b) => {
                        buf.extend_from_slice(&b);
                        Ok(buf)
                    },
                    Err(e) => Err(e.into()),
                },
            }
        })
        .await
}

/// Converts a [`Body`] to an serde deserializable object.
///
/// [`Body`]: hyper::Body
pub async fn decode_body<T>(body: &mut hyper::Body) -> Result<T, errors::Error>
where
    T: DeserializeOwned,
{
    let buf = body_to_vec(body).await?;

    serde_json::from_slice(buf.as_slice())
        .map_err(|e| Error::InvalidArgument(format!("Got error decoding body: {}", e)))
}
