use std::{
    io::{
        Read,
        Write,
    },
    path::Path,
};

use serde_json::{
    Map,
    Value,
};
use tokio::fs::File;

use super::errors::{
    Error,
    Result,
};

/// A File that can be translated to or from form data
#[derive(Debug, Clone, PartialEq, Default)]
pub struct FormDataFile {
    bytes: Vec<u8>,
    name: String,
    file_name: Option<String>,
    media_type: Option<String>,
}

impl FormDataFile {
    /// Creates a new [`FormDataFile`] from a slice of bytes, the media type and
    /// a file name.
    pub fn new(bytes: &[u8], media_type: &str, file_name: &str) -> Self {
        Self {
            bytes: bytes.to_vec(),
            name: file_name
                .split_once('.')
                .map_or("new_file", |s| s.0)
                .to_string(),
            media_type: Some(media_type.to_owned()),
            file_name: Some(file_name.to_owned()),
        }
    }

    /// Creates a new [`FormDataFile`] from a tokio [`File`] and a file name.
    pub async fn from_file(file: &mut File, file_name: &str) -> Result<Self> {
        let mut bytes = Vec::new();
        tokio::io::AsyncReadExt::read_to_end(file, &mut bytes).await?;

        Ok(Self {
            bytes,
            name: file_name
                .split_once('.')
                .map_or("new_file", |s| s.0)
                .to_owned(),
            file_name: Some(file_name.to_owned()),
            media_type: Some(get_media_type(file_name)?.to_owned()),
        })
    }

    /// Get the file extension if it exists.
    pub fn get_extension(&self) -> Option<&str> {
        self.media_type
            .as_ref()
            .map(|mt| get_extension(mt))
    }

    /// Get the file name.
    pub fn get_file_name(&self) -> &str {
        self.file_name
            .as_ref()
            .unwrap_or(&self.name)
    }

    /// get the data as a slice of u8.
    pub fn get_data(&self) -> &[u8] {
        &self.bytes
    }

    /// Parse the [`FormDataFile`] from a slice of u8.
    pub fn from_data(data: &[u8]) -> Result<Self> {
        Ok(parse_multipart_form_data(data)?
            .first()
            .unwrap()
            .clone())
    }

    /// Encode the [`FormDataFile`] to a vec of u8.
    pub fn encode(self) -> Result<Vec<u8>> {
        encode_multipart_form_data(&[self])
    }
}

/// The boundary used by the encoder.
static BOUNDARY: &str = "----------calli-cdn-form-data-boundary";

/// Encode a slice of [`FormDataFile`]s to a vec of u8.
pub fn encode_multipart_form_data(files: &[FormDataFile]) -> Result<Vec<u8>> {
    let mut data = Vec::new();

    for file in files {
        write!(&mut data, "--{}\r\n", BOUNDARY)?;

        if file
            .file_name
            .is_some()
        {
            write!(
                &mut data,
                "Content-Disposition: form-data; name=\"{}\"; filename=\"{}\"\r\n",
                file.name,
                file.file_name
                    .as_ref()
                    .unwrap()
            )?;
        } else {
            write!(
                &mut data,
                "Content-Disposition: form-data; boundary=\"{}\"\r\n",
                file.name
            )?;
        }

        if file
            .media_type
            .is_some()
        {
            write!(
                &mut data,
                "Content-Type: {}\r\n",
                file.media_type
                    .as_ref()
                    .unwrap()
            )?;
        }

        write!(&mut data, "\r\n")?;

        file.bytes
            .as_slice()
            .read_to_end(&mut data)?;

        write!(&mut data, "\r\n")?;
    }

    write!(&mut data, "--{}--\r\n", BOUNDARY)?;

    Ok(data)
}

/// Parse a slice of u8 into a vec of [`FormDataFile`]s.
pub fn parse_multipart_form_data(data: &[u8]) -> Result<Vec<FormDataFile>> {
    let mut res = Vec::new();
    let mut data_ready = false;

    if data.is_empty() {
        return Err(Error::InvalidArgument("Form data is empty".to_owned()));
    }

    for line in &mut read_lines(data) {
        if line.is_empty() && !data_ready {
            data_ready = true;
            continue;
        }

        if line.starts_with(&[b'-', b'-']) && line.ends_with(&[b'-', b'-']) {
            break;
        }

        if line.starts_with(&[b'-', b'-']) {
            data_ready = false;
            res.push(FormDataFile::default());
        }

        if res.is_empty() {
            continue;
        }

        let active_pos = res.len() - 1;
        let mut active = &mut res[active_pos];

        if line.starts_with("Content-Type: ".as_bytes()) {
            active.media_type =
                Some(String::from_utf8((&line[14..]).to_vec()).map_err(|_| {
                    Error::InvalidArgument("Invalid utf8 in media type".to_owned())
                })?);
        } else if line.starts_with("Content-Disposition: form-data;".as_bytes()) {
            let mut cd_line = &line[31..];

            while !cd_line.is_empty() {
                if cd_line.starts_with("name=\"".as_bytes()) {
                    let name = read_till_quote(&cd_line[6..]);
                    cd_line = &cd_line[(7 + name.len())..];
                    active.name = String::from_utf8(name)
                        .map_err(|_| Error::InvalidArgument("Invalid utf8 in name".to_owned()))?;
                } else if cd_line.starts_with("filename=\"".as_bytes()) {
                    let file_name = read_till_quote(&cd_line[10..]);
                    cd_line = &cd_line[(11 + file_name.len())..];
                    active.file_name = Some(String::from_utf8(file_name).map_err(|_| {
                        Error::InvalidArgument("Invalid utf8 in file name".to_owned())
                    })?);
                } else {
                    cd_line = &cd_line[1..];
                }
            }
        } else if data_ready {
            if !active
                .bytes
                .is_empty()
            {
                active
                    .bytes
                    .append(&mut b"\r\n".to_vec());
            }
            active
                .bytes
                .append(line);
        }
    }

    res = res
        .into_iter()
        .filter(|f| {
            !(f.name
                .is_empty()
                || f.bytes
                    .is_empty())
        })
        .collect();

    if res.is_empty() {
        return Err(Error::InvalidArgument(
            "No data or name in form data".to_owned(),
        ));
    }

    Ok(res)
}

/// Split a slice of u8 on `\r\n`s.
fn read_lines(data: &[u8]) -> Vec<Vec<u8>> {
    data.iter()
        .fold(vec![Vec::new()], |mut acc, b| {
            let pos = acc.len() - 1;
            match b {
                b'\n' if acc[pos].last() == Some(&b'\r') => {
                    acc[pos].pop();
                    acc.push(Vec::new());
                },
                _ => acc[pos].push(*b),
            };
            acc
        })
}

/// Read till the next `"`.
fn read_till_quote(data: &[u8]) -> Vec<u8> {
    let mut res = Vec::new();

    for b in data {
        if b == &b'"' {
            break;
        }
        res.push(*b);
    }

    res
}

/// Encode a [`File`] directly as a vec of u8 in multipart form data format.
pub async fn encode_file_as_multipart_form_data(
    file: &mut File,
    file_name: &str,
) -> Result<Vec<u8>> {
    encode_multipart_form_data(&[FormDataFile::from_file(file, file_name).await?])
}

/// Get the media type from a file name if it has an extension, defaults to
/// plain text.
fn get_media_type(file_name: &str) -> Result<&str> {
    let ext: &str = if let Some(ext) = Path::new(file_name).extension() {
        ext.to_str()
            .ok_or_else(|| {
                Error::InvalidArgument("file name contained invalid characters".to_owned())
            })?
    } else {
        ""
    };

    Ok(match ext {
        "png" => "image/png",
        "gif" => "image/gif",
        "jpg" | "jpeg" => "image/jpeg",
        "csv" => "text/csv",
        "mp4" => "video/mp4",
        "webm" => "video/webm",
        "json" => "application/json",
        "pdf" => "application/pdf",
        "zip" => "application/zip",
        "m4a" => "audio/mp4",
        "mp3" => "audio/mpeg",
        "ogg" => "audio/ogg",
        "webp" => "image/webp",
        _ => "text/plain",
    })
}

/// get the file extension from a media type, defaults to txt.
fn get_extension(media_type: &str) -> &str {
    match media_type {
        "image/png" => "png",
        "image/gif" => "gif",
        "image/jpeg" => "jpg",
        "text/csv" => "csv",
        "video/mp4" => "mp4",
        "audio/mp4" => "m4a",
        "audio/mpeg" => "mp3",
        "video/webm" => "webm",
        "application/json" => "json",
        "application/pdf" => "pdf",
        "application/zip" => "zip",
        "image/webp" => "webp",
        "audio/ogg" => "ogg",
        _ => "txt",
    }
}

/// Describes data that can be converted to a vec of [`FormDataFile`].
pub trait AsFormData {
    fn as_form_data(&self) -> Result<Vec<FormDataFile>>;
}

impl AsFormData for Value {
    fn as_form_data(&self) -> Result<Vec<FormDataFile>> {
        let map: Map<String, Value> = self
            .as_object()
            .ok_or_else(|| {
                Error::InvalidArgument("Toplevel part of form-data has to be a struct".into())
            })?
            .clone();

        let mut res = Vec::new();

        for (key, value) in map {
            if value.is_null() {
                continue;
            }
            res.push(FormDataFile {
                name: key,
                file_name: None,
                media_type: None,
                bytes: serde_json::to_string(&value)?
                    .trim_matches('"')
                    .as_bytes()
                    .to_vec(),
            });
        }

        Ok(res)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn encode_decode_form_data() {
        let payload =
            b"Some Test data of text\r\nowo\r\n\r\n\nmaybe multiple lines of it too lol uwu";

        println!("{}", String::from_utf8(payload.to_vec()).unwrap());

        let encoded =
            encode_multipart_form_data(&vec![FormDataFile::new(payload, "text/plain", "test.txt")])
                .unwrap();

        // println!("{}", String::from_utf8(encoded.clone()).unwrap());

        let decoded = parse_multipart_form_data(&encoded)
            .unwrap()
            .first()
            .unwrap()
            .clone();

        println!(
            "{}",
            String::from_utf8(
                decoded
                    .get_data()
                    .to_vec()
            )
            .unwrap()
        );

        assert_eq!(decoded.bytes, payload);
        assert_eq!(decoded.name, "test".to_owned());
        assert_eq!(decoded.file_name, Some("test.txt".to_owned()));
        assert_eq!(decoded.media_type, Some("text/plain".to_owned()));
    }
}
