use std::{
    path::PathBuf,
    str::FromStr,
};

use crate::utils::{
    constants::FILE_DIR,
    errors::Error,
};

/// Represents a path to a file in the file server.
#[derive(Debug, Clone)]
pub struct Path {
    path: PathBuf,
    file_name: String,
}

impl Path {
    /// Returns the file ext of the file the path points to if it has one.
    pub fn file_ext(&self) -> Option<&str> {
        self.file_name
            .split('.')
            .skip(1)
            .last()
    }

    /// Create a `Path` from a std `std::path::Path`.
    pub fn from_path(path: impl AsRef<std::path::Path>) -> Self {
        let path = path.as_ref();
        Self {
            file_name: path
                .file_name()
                .map(|s| {
                    s.to_str()
                        .expect("Invalid file name")
                        .to_owned()
                })
                .expect("Path without a filename was passed to a path for a file"),
            path: path.to_owned(),
        }
    }

    /// Add another `std::path::Path` onto this path.
    pub fn add_onto(&mut self, path: impl AsRef<std::path::Path>) -> &mut Self {
        self.path = self
            .path
            .join(path);
        self
    }
}

impl FromStr for Path {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Error> {
        let file_name = s
            .split('/')
            .last()
            .ok_or_else(|| Error::InvalidArgument("Path too short".to_owned()))?
            .to_owned();

        Ok(Self {
            file_name,
            path: std::path::Path::new(&*FILE_DIR).join(&s.trim_matches('/')),
        })
    }
}

impl std::fmt::Display for Path {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(
            &(self
                .path
                .to_str()
                .unwrap()
                .strip_prefix(&*FILE_DIR)
                .unwrap()
                .trim_end_matches('/')
                .to_owned()
                + "/"),
        )
    }
}

impl AsRef<std::path::Path> for Path {
    fn as_ref(&self) -> &std::path::Path {
        self.path
            .as_ref()
    }
}
