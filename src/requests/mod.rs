use std::{
    fmt::{
        self,
        Debug,
    },
    future::Future,
    pin::Pin,
};

/// Returns Ok contents if successful, else turns Err contents into an
/// [`Error`], writes it to resp and returns.
///
/// [`Error`]: crate::utils::Error
macro_rules! use_err_for_resp {
    ($resp:expr, $e:expr) => {
        match $e {
            Ok(r) => r,
            Err(e) => {
                let e: crate::utils::Error = e.into();
                println!("Handling error {:?}", &e);
                e.make_resp(&mut $resp);
                return;
            },
        }
    };
}

mod authentication;
mod files;
mod folders;
mod handlers;
mod paths;
mod users;

pub use handlers::make_router_service;

pub mod endpoints {
    pub use super::authentication::*;
    pub use super::files::*;
    pub use super::folders::*;
    pub use super::users::*;
}

use http::Method;
use hyper::{
    Body,
    Request,
    Response,
};
pub(crate) use paths::Path;

use crate::config::ConfigStorageArc;

pub type RouteOut<'a> = Pin<Box<dyn Future<Output = ()> + Send + 'a>>;
type RouteFunc = fn(Request<Body>, &mut Response<Body>, ConfigStorageArc) -> RouteOut;

#[derive(Clone)]
pub struct Endpoint {
    method: Method,
    path: &'static str,
    use_rest: bool,
    func: RouteFunc,
}

impl Debug for Endpoint {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Endpoint")
            .field("method", &self.method)
            .field("path", &self.path)
            .field("use_rest", &self.use_rest)
            .finish()
    }
}
