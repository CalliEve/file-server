use hyper::{
    Body,
    Request,
    Response,
};
use serde::{
    Deserialize,
    Serialize,
};

use crate::{
    authentication,
    config::ConfigStorageArc,
    endpoint,
    utils::decode_body,
};

/// Data to be received from an authentication request.
#[derive(Deserialize)]
struct AuthReq {
    username: String,
    password: String,
}

/// Data to be send back in response to a succesful authentication request.
#[derive(Serialize)]
struct AuthResp {
    token: String,
}

/// Handles an authentication request.
#[endpoint(POST "/authenticate")]
pub async fn authenticate(
    mut req: Request<Body>,
    mut resp: &mut Response<Body>,
    cfg: ConfigStorageArc,
) {
    let a_req: AuthReq = use_err_for_resp!(resp, decode_body(req.body_mut()).await);

    let body = AuthResp {
        token: use_err_for_resp!(
            resp,
            authentication::create_token(&a_req.username, &a_req.password, &cfg).await
        ),
    };

    *resp.body_mut() = use_err_for_resp!(resp, serde_json::to_vec_pretty(&body)).into();
    *resp.status_mut() = hyper::StatusCode::OK;
}
