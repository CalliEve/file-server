use std::{
    convert::Infallible,
    future::Future,
    pin::Pin,
    sync::Arc,
    task::{
        Context,
        Poll,
    },
};

use hyper::{
    service::Service,
    Body,
    Request,
    Response,
};

use super::{
    Endpoint,
    RouteFunc,
};
use crate::{
    authentication::is_admin,
    config::{
        ConfigStorage,
        ConfigStorageArc,
    },
    Error,
};

type PinnedResponseFuture<T> = Pin<Box<dyn Future<Output = Result<T, hyper::Error>> + Send>>;

pub struct RouterService {
    data_store: ConfigStorageArc,
    endpoints: Vec<Endpoint>,
}

impl Service<Request<Body>> for RouterService {
    type Response = Response<Body>;
    type Error = hyper::Error;
    type Future = PinnedResponseFuture<Self::Response>;

    fn poll_ready(&mut self, _: &mut Context) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Request<Body>) -> Self::Future {
        let router = Router::new(
            self.data_store
                .clone(),
            self.endpoints
                .clone(),
        );
        Box::pin(async move {
            Ok(router
                .handle_req(req)
                .await
                .unwrap())
        })
    }
}

pub struct MakeRouterService {
    data_store: ConfigStorageArc,
    endpoints: Vec<Endpoint>,
}

impl<'a, T> Service<T> for MakeRouterService {
    type Response = RouterService;
    type Error = hyper::Error;
    type Future = PinnedResponseFuture<Self::Response>;

    fn poll_ready(&mut self, _: &mut Context) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, _: T) -> Self::Future {
        let router = RouterService {
            data_store: self
                .data_store
                .clone(),
            endpoints: self
                .endpoints
                .clone(),
        };
        Box::pin(async move { Ok(router) })
    }
}

pub fn make_router_service<S>(store: S, endpoints: Vec<Endpoint>) -> MakeRouterService
where
    S: ConfigStorage + Send + Sync + 'static,
{
    MakeRouterService {
        data_store: Arc::new(store),
        endpoints,
    }
}

#[derive(Debug)]
enum RouteMatch {
    Some(usize, Endpoint),
    None,
}

impl RouteMatch {
    fn new(e: Endpoint) -> Self {
        let l = if e.path == "/" {
            0
        } else {
            e.path
                .matches('/')
                .count()
        };
        Self::Some(l, e)
    }

    fn is_none(&self) -> bool {
        match self {
            Self::None => true,
            Self::Some(_, _) => false,
        }
    }

    fn length(&self) -> usize {
        match self {
            Self::Some(l, _) => *l,
            Self::None => 0,
        }
    }

    fn get_func(self) -> Option<RouteFunc> {
        match self {
            Self::Some(_, e) => Some(e.func),
            Self::None => None,
        }
    }
}

struct Router {
    data_store: ConfigStorageArc,
    endpoints: Vec<Endpoint>,
}

impl Router {
    fn new(data_store: ConfigStorageArc, endpoints: Vec<Endpoint>) -> Self {
        Self {
            data_store,
            endpoints,
        }
    }

    /// Handles incoming requests and returns their response.
    ///
    /// Only GET and POST methods are supported, all others return a
    /// `METHOD_NOT_ALLOWED`.
    ///
    /// All requests outside /authenticate and /admin/* are assumed to be
    /// requests for files.
    pub async fn handle_req(&self, req: Request<Body>) -> Result<Response<Body>, Infallible> {
        let mut response = Response::new(Body::empty());

        if req
            .uri()
            .path()
            .starts_with("/admin")
        {
            self.handle_admin_req(req, &mut response)
                .await;
        } else {
            self.dispatch_endpoint(req, &mut response)
                .await;
        }

        Ok(response)
    }

    /// Handles a request to an admin endpoint.
    ///
    /// Checks if the user has admin permissions and then passes the request on.
    pub async fn handle_admin_req(&self, req: Request<Body>, mut resp: &mut Response<Body>) {
        use_err_for_resp!(resp, is_admin(&req, &self.data_store).await);

        self.dispatch_endpoint(req, resp)
            .await;
    }

    async fn dispatch_endpoint(&self, req: Request<Body>, mut resp: &mut Response<Body>) {
        let path = req
            .uri()
            .path()
            .trim_end_matches('/')
            .to_string();
        let method = req
            .method()
            .clone();

        let found = self
            .endpoints
            .iter()
            .filter(|e| e.method == method)
            .fold(RouteMatch::None, |acc, endpoint| {
                if path == endpoint.path || (endpoint.use_rest && path.starts_with(endpoint.path)) {
                    let new = RouteMatch::new(endpoint.clone());
                    if acc.length() < new.length() || acc.is_none() {
                        return new;
                    }
                }
                acc
            });

        if let Some(f) = found.get_func() {
            return f(
                req,
                resp,
                self.data_store
                    .clone(),
            )
            .await;
        }

        println!("No endpoint found");
        use_err_for_resp!(resp, Err(Error::NotFound));
    }
}
