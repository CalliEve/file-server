use glob::Pattern;
use hyper::{
    Body,
    Request,
    Response,
    StatusCode,
};
use serde::Deserialize;

use super::Path;
use crate::{
    authentication::{
        get_current_username,
        has_access_to_path,
    },
    config::ConfigStorageArc,
    endpoint,
    fs::{
        create_folder,
        delete_folder,
        get_folder_contents,
    },
    utils::{
        body_to_vec,
        constants::FILE_DIR,
        Error,
    },
};

#[derive(Deserialize)]
pub struct FolderRequest {
    path: String,
    #[serde(default)]
    data: FolderRequestData,
}

impl FolderRequest {
    fn full_path(&self) -> String {
        format!(
            "{}/{}",
            *FILE_DIR,
            self.path
                .trim_matches('/')
        )
    }

    fn to_pattern(&self) -> Result<Pattern, Error> {
        let path = String::from("/")
            + &self
                .path
                .trim_matches('/')
                .to_string();

        let is_complete_pat = path
            .split('/')
            .last()
            .map_or(false, |s| {
                s.trim_matches('.')
                    .contains('.')
                    || s.ends_with('*')
            });

        if is_complete_pat {
            Pattern::new(&path).map_err(|_| {
                Error::InvalidArgument(
                    "Invalid path; can't be turned into a glob pattern".to_owned(),
                )
            })
        } else {
            Pattern::new(&(path + "/*")).map_err(|_| {
                Error::InvalidArgument(
                    "Invalid path; can't be turned into a glob pattern".to_owned(),
                )
            })
        }
    }
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum FolderRequestData {
    UserTarget { name: String },
    None,
}

impl Default for FolderRequestData {
    fn default() -> Self {
        Self::None
    }
}

async fn decode_body(req: &mut Request<Body>) -> Result<FolderRequest, Error> {
    serde_json::from_slice(&body_to_vec(req.body_mut()).await?).map_err(Into::into)
}

#[endpoint(POST "/folders")]
pub async fn post_folder_endpoint(
    mut req: Request<Body>,
    mut resp: &mut Response<Body>,
    cfg: ConfigStorageArc,
) {
    let body = use_err_for_resp!(resp, decode_body(&mut req).await);

    let path = body.full_path();

    use_err_for_resp!(resp, has_access_to_path(&req, &path, &cfg).await);

    use_err_for_resp!(resp, create_folder(&path).await);

    let user = use_err_for_resp!(resp, get_current_username(&req).await);

    use_err_for_resp!(
        resp,
        cfg.add_access(&user, Pattern::new(&(path + "/*.*")).unwrap())
            .await
    );

    *resp.status_mut() = StatusCode::NO_CONTENT;
}

#[endpoint(DELETE "/folders")]
pub async fn delete_folder_endpoint(
    mut req: Request<Body>,
    mut resp: &mut Response<Body>,
    cfg: ConfigStorageArc,
) {
    let body = use_err_for_resp!(resp, decode_body(&mut req).await);

    let path = body.full_path();

    use_err_for_resp!(resp, has_access_to_path(&req, &path, &cfg).await);

    use_err_for_resp!(
        resp,
        cfg.delete_path(&path)
            .await
    );

    use_err_for_resp!(resp, delete_folder(&path).await);

    *resp.status_mut() = StatusCode::NO_CONTENT;
}

#[endpoint(POST "/folders/public")]
pub async fn make_public_endpoint(
    mut req: Request<Body>,
    mut resp: &mut Response<Body>,
    cfg: ConfigStorageArc,
) {
    let body = use_err_for_resp!(resp, decode_body(&mut req).await);

    let path = body.full_path();

    use_err_for_resp!(resp, has_access_to_path(&req, &path, &cfg).await);

    let pat = use_err_for_resp!(resp, body.to_pattern());

    use_err_for_resp!(
        resp,
        cfg.add_public_path(pat)
            .await
    );

    *resp.status_mut() = StatusCode::NO_CONTENT;
}

#[endpoint(DELETE "/folders/public")]
pub async fn hide_public_endpoint(
    mut req: Request<Body>,
    mut resp: &mut Response<Body>,
    cfg: ConfigStorageArc,
) {
    let body = use_err_for_resp!(resp, decode_body(&mut req).await);

    let path = body.full_path();

    use_err_for_resp!(resp, has_access_to_path(&req, &path, &cfg).await);

    let pat = use_err_for_resp!(resp, body.to_pattern());

    use_err_for_resp!(
        resp,
        cfg.remove_public_path(&pat)
            .await
    );

    *resp.status_mut() = StatusCode::NO_CONTENT;
}

async fn has_access_to_path_bool(cfg: &ConfigStorageArc, req: &Request<Body>, path: &Path) -> bool {
    if cfg
        .is_public(&path.to_string())
        .await
    {
        return true;
    }

    if has_access_to_path(req, path.to_string(), cfg)
        .await
        .is_ok()
    {
        return true;
    }

    false
}

/// Handles a request to get a list of all contents of a directory.
pub async fn handle_get_folder<'a>(
    cfg: ConfigStorageArc,
    path: &'a Path,
    req: Request<Body>,
    mut resp: &'a mut Response<Body>,
) {
    let mut contents = use_err_for_resp!(resp, get_folder_contents(path).await);

    if !has_access_to_path_bool(&cfg, &req, path).await {
        let mut with_access = Vec::new();

        for c in contents {
            if has_access_to_path_bool(
                &cfg,
                &req,
                path.clone()
                    .add_onto(&c.name),
            )
            .await
            {
                with_access.push(c);
            }
        }

        if with_access.is_empty() {
            use_err_for_resp!(resp, Err(Error::NotAllowed));
        }

        contents = with_access;
    }

    *resp.body_mut() = use_err_for_resp!(resp, serde_json::to_vec_pretty(&contents)).into();
    *resp.status_mut() = StatusCode::OK;
}
