use std::str::FromStr;

use hyper::{
    header::{
        HeaderValue,
        CONTENT_TYPE,
    },
    Body,
    Request,
    Response,
    StatusCode,
};

use super::{
    endpoints::handle_get_folder,
    Path,
};
use crate::{
    authentication::has_access,
    config::ConfigStorageArc,
    endpoint,
    fs::{
        create_file,
        create_formdata_file,
        delete_file,
        find_matching_file,
        get_file,
        get_file_as_form_data,
        is_dir,
    },
    utils::errors::Error,
};

/// Handles an incoming GET request for a file.
#[endpoint(GET "/", use_rest=true)]
pub async fn get_file_endpoint(
    req: Request<Body>,
    mut resp: &mut Response<Body>,
    cfg: ConfigStorageArc,
) {
    let path = use_err_for_resp!(
        resp,
        Path::from_str(
            req.uri()
                .path()
        )
    );

    if is_dir(&path) {
        handle_get_folder(cfg, &path, req, resp).await;
        return;
    }

    if !cfg
        .is_public(&path.to_string())
        .await
    {
        use_err_for_resp!(resp, has_access(&req, &cfg).await);
    }

    match path.file_ext() {
        Some(_) => handle_get_raw_file(&path, resp).await,
        None => handle_get_form_data_file(&path, resp).await,
    }
}

/// Handles an incoming POST request for a file.
#[endpoint(POST "/", use_rest=true)]
pub async fn post_file_endpoint(
    req: Request<Body>,
    mut resp: &mut Response<Body>,
    cfg: ConfigStorageArc,
) {
    use_err_for_resp!(resp, has_access(&req, &cfg).await);

    let path = use_err_for_resp!(
        resp,
        Path::from_str(
            req.uri()
                .path()
        )
    );

    match path.file_ext() {
        Some(_) => handle_post_raw_file(&path, req.into_body(), resp).await,
        None => handle_post_form_data_file(&path, req.into_body(), resp).await,
    }
}

/// Handles a request that wants a raw file.
pub async fn handle_get_raw_file(path: &Path, mut resp: &mut Response<Body>) {
    let data = use_err_for_resp!(resp, get_file(path).await);

    *resp.body_mut() = data.into();
    *resp.status_mut() = StatusCode::OK;
}

/// Handles a request that wants a file as form data.
pub async fn handle_get_form_data_file(path: &Path, mut resp: &mut Response<Body>) {
    let path_opt = use_err_for_resp!(resp, find_matching_file(path));
    let path = use_err_for_resp!(resp, path_opt.ok_or(Error::NotFound));

    let data = use_err_for_resp!(resp, get_file_as_form_data(path).await);

    *resp.body_mut() = use_err_for_resp!(resp, data.encode()).into();
    resp.headers_mut()
        .insert(
            CONTENT_TYPE,
            HeaderValue::from_static("multipart/form-data"),
        );
    *resp.status_mut() = StatusCode::OK;
}

/// Handles a request that posts a file as raw bytes.
pub async fn handle_post_raw_file(path: &Path, body: Body, mut resp: &mut Response<Body>) {
    use_err_for_resp!(resp, create_file(path, body).await);

    *resp.status_mut() = StatusCode::NO_CONTENT;
}

/// Handles a request that posts a file as form data.
pub async fn handle_post_form_data_file(path: &Path, body: Body, mut resp: &mut Response<Body>) {
    use_err_for_resp!(resp, create_formdata_file(path, body).await);

    *resp.status_mut() = StatusCode::NO_CONTENT;
}

#[endpoint(DELETE "/", use_rest=true)]
pub async fn delete_file_endpoint(
    req: Request<Body>,
    mut resp: &mut Response<Body>,
    cfg: ConfigStorageArc,
) {
    use_err_for_resp!(resp, has_access(&req, &cfg).await);

    let path_obj = use_err_for_resp!(
        resp,
        Path::from_str(
            req.uri()
                .path()
        )
    );

    let path = match path_obj.file_ext() {
        Some(_) => path_obj,
        None => {
            let path_opt = use_err_for_resp!(resp, find_matching_file(path_obj));
            Path::from_path(use_err_for_resp!(resp, path_opt.ok_or(Error::NotFound)))
        },
    };

    use_err_for_resp!(resp, delete_file(path).await);

    *resp.status_mut() = StatusCode::NO_CONTENT;
}
