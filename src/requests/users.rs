use glob::Pattern;
use hyper::{
    Body,
    Request,
    Response,
};
use serde::Deserialize;

use crate::{
    authentication,
    config::{
        ConfigStorageArc,
        User,
    },
    endpoint,
    utils::{
        decode_body,
        serde_glob_handling,
        Error,
    },
};

fn get_username(path: &str) -> &str {
    path.trim_start_matches("/admin/user")
}

/// Data to be send with a create user request.
#[derive(Deserialize)]
struct UserCreateReq {
    username: String,
    password: String,
    #[serde(with = "serde_glob_handling::with_vec")]
    access: Vec<Pattern>,
}

/// Handles a create user request.
#[endpoint(POST "/admin/user")]
pub async fn create_user(
    mut req: Request<Body>,
    mut resp: &mut Response<Body>,
    cfg: ConfigStorageArc,
) {
    let user: UserCreateReq = use_err_for_resp!(resp, decode_body(req.body_mut()).await);

    let password = authentication::encrypt(&user.username, &user.password, &cfg).await;
    use_err_for_resp!(
        resp,
        cfg.add_user(User {
            name: user.username,
            password: password.to_vec(),
            access: user.access
        })
        .await
    );

    *resp.status_mut() = hyper::StatusCode::NO_CONTENT;
}

/// Data to be send with a create user request.
#[derive(Deserialize)]
struct UserUpdateReq {
    #[serde(default)]
    username: Option<String>,
    #[serde(default)]
    password: Option<String>,
    #[serde(default)]
    #[serde(with = "serde_glob_handling::with_vec")]
    access: Vec<Pattern>,
}

/// Handles a request to update an user.
#[endpoint(PATCH "/admin/user", use_rest=true)]
pub async fn update_user(
    mut req: Request<Body>,
    mut resp: &mut Response<Body>,
    cfg: ConfigStorageArc,
) {
    let username = get_username(
        req.uri()
            .path()
            .rsplit_once('/')
            .map(|s| s.1)
            .expect("No path components found"),
    );
    let old_user = if let Some(u) = cfg
        .get_user(username)
        .await
    {
        u
    } else {
        use_err_for_resp!(resp, Err(Error::NotFound))
    };

    let user: UserUpdateReq = use_err_for_resp!(resp, decode_body(req.body_mut()).await);

    let new_username = user
        .username
        .clone()
        .unwrap_or_else(|| {
            old_user
                .name
                .clone()
        });
    let password = if let Some(pw) = &user.password {
        authentication::encrypt(&new_username, pw, &cfg)
            .await
            .to_vec()
    } else {
        old_user
            .password
            .clone()
    };

    let access = if user
        .access
        .is_empty()
    {
        old_user
            .access
            .clone()
    } else {
        user.access
    };

    use_err_for_resp!(
        resp,
        cfg.replace_user(
            &old_user.name,
            User {
                name: new_username,
                password,
                access,
            }
        )
        .await
    );

    *resp.status_mut() = hyper::StatusCode::NO_CONTENT;
}

/// Handles a request to delete an user.
#[endpoint(DELETE "/admin/user", use_rest=true)]
pub async fn delete_user(req: Request<Body>, mut resp: &mut Response<Body>, cfg: ConfigStorageArc) {
    use_err_for_resp!(
        resp,
        cfg.delete_user(get_username(
            req.uri()
                .path()
                .rsplit_once('/')
                .map(|s| s.1)
                .expect("No path components found")
        ))
        .await
    );

    *resp.status_mut() = hyper::StatusCode::NO_CONTENT;
}
