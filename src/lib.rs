#![warn(clippy::pedantic)]
#![allow(
    dead_code,
    clippy::missing_errors_doc,
    clippy::module_name_repetitions,
    clippy::single_match_else
)]

mod authentication;
mod config;
mod fs;
mod requests;
mod utils;

pub use config::{
    config_file::ConfigFile,
    ConfigStorage,
    User,
};
pub use framework_macros::{
    endpoint,
    routes,
};
pub use requests::{
    endpoints,
    make_router_service,
    Endpoint,
};
pub use utils::Error;
