use std::sync::Arc;

use async_trait::async_trait;
use glob::Pattern;
use serde::{
    Deserialize,
    Serialize,
};

use crate::utils::{
    errors::Error,
    serde_glob_handling,
};

pub mod config_file;

/// The representation of an user as saved in the config.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct User {
    pub name: String,
    pub password: Vec<u8>,
    #[serde(with = "serde_glob_handling::with_vec")]
    pub access: Vec<Pattern>,
}

/// The tait that describes a storage solution for the server's config
#[async_trait]
pub trait ConfigStorage {
    /// Gets the salt used for encrypting passwords
    async fn get_salt(&self) -> [u8; 16];

    /// Gets an user by their username
    async fn get_user(&self, username: &str) -> Option<User>;

    /// Adds an user
    async fn add_user(&self, user: User) -> Result<(), Error>;

    /// Replaces an user
    async fn replace_user(&self, username: &str, replacement: User) -> Result<(), Error>;

    /// Deletes an user
    async fn delete_user(&self, username: &str) -> Result<(), Error>;

    /// Returns if a given path is marked as public.
    async fn is_public(&self, path: &str) -> bool;

    /// Adds a [`Pattern`] as a public path.
    async fn add_public_path(&self, pat: Pattern) -> Result<(), Error>;

    /// Removes a [`Pattern`] from being public.
    async fn remove_public_path(&self, pat: &Pattern) -> Result<(), Error>;

    /// Adds a [`Pattern`] for access to an user.
    async fn add_access(&self, username: &str, pat: Pattern) -> Result<(), Error>;

    /// Removes a [`Pattern`] from being accessible for an user.
    async fn remove_access(&self, username: &str, pat: &Pattern) -> Result<(), Error>;

    /// Removes all [`Pattern`]s from all users that include this path.
    async fn delete_path(&self, path: &str) -> Result<(), Error>;

    /// Returns if an user is admin or not
    async fn is_admin(&self, username: &str) -> bool;
}

pub type ConfigStorageArc = Arc<dyn ConfigStorage + Send + Sync>;
