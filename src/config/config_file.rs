use std::sync::Arc;

use async_trait::async_trait;
use glob::Pattern;
use serde::{
    Deserialize,
    Serialize,
};
use tokio::{
    fs::File,
    io::{
        AsyncReadExt,
        AsyncWriteExt,
    },
    sync::RwLock,
};

use super::{
    ConfigStorage,
    ConfigStorageArc,
    User,
};
use crate::{
    authentication::encrypt,
    utils::{
        errors::Error,
        serde_glob_handling,
    },
};

/// The config.
#[derive(Deserialize, Serialize)]
struct Config {
    users: Vec<User>,
    salt: [u8; 16],
    #[serde(default)]
    #[serde(with = "serde_glob_handling::with_vec")]
    public_paths: Vec<Pattern>,
}

#[derive(Clone)]
pub struct ConfigFile {
    config: Arc<RwLock<Config>>,
    path: String,
}

impl ConfigFile {
    pub async fn new() -> Result<Self, Error> {
        Self::new_from_file(
            std::env::var("CDN_CONFIG").unwrap_or_else(|_| "./config.json".to_owned()),
        )
        .await
    }

    // Allow because the panic warning is for creating glob patterns that are
    // guaranteed to work
    #[allow(clippy::missing_panics_doc)]
    pub async fn new_from_file(path: String) -> Result<Self, Error> {
        let mut bytes = Vec::new();

        File::open(&path)
            .await?
            .read_to_end(&mut bytes)
            .await?;

        let config = Arc::new(RwLock::new(
            serde_json::from_slice::<Config>(&bytes).map_err(Error::from)?,
        ));

        let res = Self {
            config,
            path,
        };

        if std::env::var("CDN_DEVELOPMENT").map_or(false, |e| e == "1")
            && res
                .get_user("admin")
                .await
                .is_none()
        {
            println!("Running in development mode; adding debug admin account");
            res.add_user(User {
                name: "admin".to_owned(),
                password: encrypt(
                    "admin",
                    "debug",
                    &(Arc::new(res.clone()) as ConfigStorageArc),
                )
                .await
                .to_vec(),
                access: vec![
                    Pattern::new("*/*").unwrap(),
                    Pattern::new("/admin").unwrap(),
                ],
            })
            .await
            .expect("Encountered error while adding debug admin account");
            res.update_config_file()
                .await
                .expect("Encountered error when writing debug admin update to config");
        }

        Ok(res)
    }

    /// Updates the config file with the new state of the config.
    async fn update_config_file(&self) -> Result<(), Error> {
        let data = serde_json::to_vec(
            &*self
                .config
                .read()
                .await,
        )?;
        File::create(&self.path)
            .await?
            .write_all(&data)
            .await?;

        Ok(())
    }
}

#[async_trait]
impl ConfigStorage for ConfigFile {
    async fn get_salt(&self) -> [u8; 16] {
        self.config
            .read()
            .await
            .salt
    }

    async fn get_user(&self, username: &str) -> Option<User> {
        self.config
            .read()
            .await
            .users
            .iter()
            .find(|u| u.name == username)
            .cloned()
    }

    async fn add_user(&self, user: User) -> Result<(), Error> {
        if self
            .get_user(&user.name)
            .await
            .is_some()
        {
            return Err(Error::AlreadyExists);
        }

        self.config
            .write()
            .await
            .users
            .push(user);

        self.update_config_file()
            .await
    }

    async fn replace_user(&self, username: &str, replacement: User) -> Result<(), Error> {
        let mut found = false;

        if username != replacement.name
            && self
                .get_user(&replacement.name)
                .await
                .is_some()
        {
            return Err(Error::AlreadyExists);
        }

        for user in &mut self
            .config
            .write()
            .await
            .users
        {
            if username == user.name {
                *user = replacement;
                found = true;
                break;
            }
        }

        if !found {
            return Err(Error::NotFound);
        }

        self.update_config_file()
            .await
    }

    async fn delete_user(&self, username: &str) -> Result<(), Error> {
        {
            let users = &mut self
                .config
                .write()
                .await
                .users;

            users.remove(
                users
                    .iter()
                    .position(|u| u.name == username)
                    .ok_or(Error::NotFound)?,
            );
        }

        self.update_config_file()
            .await
    }

    async fn is_public(&self, path: &str) -> bool {
        self.config
            .read()
            .await
            .public_paths
            .iter()
            .any(|p| p.matches(path))
    }

    async fn add_public_path(&self, pat: Pattern) -> Result<(), Error> {
        {
            let paths = &mut self
                .config
                .write()
                .await
                .public_paths;

            if paths.contains(&pat) {
                return Err(Error::AlreadyExists);
            }

            paths.push(pat);
        }

        self.update_config_file()
            .await
    }

    async fn remove_public_path(&self, pat: &Pattern) -> Result<(), Error> {
        {
            let paths = &mut self
                .config
                .write()
                .await
                .public_paths;

            paths.remove(
                paths
                    .iter()
                    .position(|p| p == pat)
                    .ok_or(Error::NotFound)?,
            );
        }

        self.update_config_file()
            .await
    }

    async fn add_access(&self, username: &str, pat: Pattern) -> Result<(), Error> {
        self.config
            .write()
            .await
            .users
            .iter_mut()
            .find(|u| u.name == username)
            .ok_or(Error::NotFound)?
            .access
            .push(pat);

        self.update_config_file()
            .await
    }

    async fn remove_access(&self, username: &str, pat: &Pattern) -> Result<(), Error> {
        {
            let users = &mut self
                .config
                .write()
                .await
                .users;

            let access = &mut users
                .iter_mut()
                .find(|u| u.name == username)
                .ok_or(Error::NotFound)?
                .access;

            access.remove(
                access
                    .iter()
                    .position(|p| p == pat)
                    .ok_or(Error::NotFound)?,
            );
        }

        self.update_config_file()
            .await
    }

    async fn delete_path(&self, path: &str) -> Result<(), Error> {
        {
            let config = &mut self
                .config
                .write()
                .await;

            for user in &mut config.users {
                user.access = user
                    .access
                    .iter()
                    .filter(|p| {
                        !p.to_string()
                            .starts_with(path)
                    })
                    .cloned()
                    .collect();
            }

            config.public_paths = config
                .public_paths
                .iter()
                .filter(|p| {
                    !p.to_string()
                        .starts_with(path)
                })
                .cloned()
                .collect();
        }

        self.update_config_file()
            .await
    }

    async fn is_admin(&self, username: &str) -> bool {
        if let Some(user) = self
            .get_user(username)
            .await
        {
            if user
                .access
                .contains(&Pattern::new("/admin").unwrap())
            {
                return true;
            }
        }

        false
    }
}
