use std::path::Path;

use futures::stream::StreamExt;
use hyper::Body;
use tokio::{
    fs::{
        create_dir_all,
        OpenOptions,
    },
    io::AsyncWriteExt,
};

use crate::utils::{
    body_to_vec,
    errors::{
        Error,
        Result,
    },
    form_data::FormDataFile,
};

/// Creates a file at the given path with as its content the bytes in the
/// request body.
pub async fn create_file(path: impl AsRef<Path>, mut body: Body) -> Result<()> {
    let mut file = OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(path)
        .await?;

    while let Some(bytes) = body
        .next()
        .await
    {
        let bytes = bytes?;

        file.write_all(&bytes)
            .await?;
    }

    Ok(())
}

/// Creates a file at the given path with its contents the first file in a
/// decoded form data request body.
pub async fn create_formdata_file(path: impl AsRef<Path>, mut body: Body) -> Result<()> {
    let form_data = FormDataFile::from_data(&body_to_vec(&mut body).await?)?;

    let full_path = path
        .as_ref()
        .with_extension(
            form_data
                .get_extension()
                .ok_or_else(|| {
                    Error::InvalidArgument("No media type specified for file".to_owned())
                })?,
        );

    let mut file = OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(full_path)
        .await?;

    file.write_all(form_data.get_data())
        .await?;

    Ok(())
}

/// Creates a folder and all parent folders for the given path.
pub async fn create_folder(path: impl AsRef<Path>) -> Result<()> {
    create_dir_all(path)
        .await
        .map_err(Into::into)
}
