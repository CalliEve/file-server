use std::path::Path;

use tokio::fs::{
    remove_dir_all,
    remove_file,
};

use crate::utils::Error;

/// Deletes a file from the file system.
pub async fn delete_file(path: impl AsRef<Path>) -> Result<(), Error> {
    remove_file(path)
        .await
        .map_err(Into::into)
}

/// Deletes a folder **and contents** from the file system.
pub async fn delete_folder(path: impl AsRef<Path>) -> Result<(), Error> {
    remove_dir_all(path)
        .await
        .map_err(Into::into)
}
