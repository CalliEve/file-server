use std::{
    borrow::ToOwned,
    path::Path,
};

use futures::future::{
    BoxFuture,
    FutureExt,
};
use tokio::fs::read_dir;

use crate::utils::{
    constants::FILE_DIR,
    errors::Result,
};

/// Finds a file matching the given name and returns its path as a string.
pub fn find_matching_file(name: impl AsRef<Path>) -> Result<Option<String>> {
    let parent = name
        .as_ref()
        .parent()
        .unwrap_or_else(|| Path::new(&*FILE_DIR));

    let stem = name
        .as_ref()
        .file_stem();

    Ok(parent
        .read_dir()?
        .filter_map(std::io::Result::ok)
        .find_map(|entry| {
            if entry
                .path()
                .file_stem()
                == stem
            {
                Some(
                    entry
                        .path()
                        .to_str()
                        .map(ToOwned::to_owned),
                )
            } else {
                None
            }
        })
        .flatten())
}

pub fn find_all_nested_dirs(path: impl AsRef<Path>) -> BoxFuture<'static, Result<Vec<String>>> {
    let path = path
        .as_ref()
        .to_owned();
    async move {
        let mut dirs = Vec::new();

        let mut dir_content = read_dir(path).await?;
        while let Ok(Some(entry)) = dir_content
            .next_entry()
            .await
        {
            match entry
                .file_type()
                .await
            {
                Ok(ft) if ft.is_dir() => {
                    dirs.push(
                        entry
                            .path()
                            .to_string_lossy()
                            .into_owned(),
                    );
                    dirs.append(&mut find_all_nested_dirs(entry.path()).await?);
                },
                _ => {},
            }
        }

        Ok(dirs)
    }
    .boxed()
}

/// Returns if the given directory points to a directory
pub fn is_dir(path: impl AsRef<Path>) -> bool {
    path.as_ref()
        .is_dir()
}
