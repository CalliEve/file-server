use std::path::Path;

use serde::Serialize;
use tokio::{
    fs::{
        read_dir,
        File,
    },
    io::AsyncReadExt,
};

use crate::utils::{
    errors::Result,
    form_data::FormDataFile,
};

/// Describes an item in a directory.
#[derive(Debug, Clone, Serialize)]
pub struct DirItem {
    pub name: String,
    pub is_dir: bool,
}

/// Returns the contents of a file at the given path as bytes.
pub async fn get_file(path: impl AsRef<Path>) -> Result<Vec<u8>> {
    let mut file = File::open(path).await?;

    let mut res = Vec::new();

    file.read_to_end(&mut res)
        .await?;

    Ok(res)
}

/// Returns the contents of a file at the given path as a [`FormDataFile`].
pub async fn get_file_as_form_data(path: impl AsRef<Path>) -> Result<FormDataFile> {
    let mut file = File::open(&path).await?;

    FormDataFile::from_file(
        &mut file,
        path.as_ref()
            .file_name()
            .unwrap()
            .to_str()
            .unwrap(),
    )
    .await
}

/// Returns a list with all items in the given directory.
pub async fn get_folder_contents(path: impl AsRef<Path>) -> Result<Vec<DirItem>> {
    let mut contents = Vec::new();

    let mut dir_content = read_dir(path).await?;
    while let Ok(Some(entry)) = dir_content
        .next_entry()
        .await
    {
        contents.push(DirItem {
            name: entry
                .file_name()
                .to_str()
                .expect("encountered invalid file name")
                .to_owned(),
            is_dir: entry
                .file_type()
                .await
                .expect("Can't get file type for directory entry")
                .is_dir(),
        });
    }

    Ok(contents)
}
