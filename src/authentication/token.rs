use std::collections::HashMap;

use once_cell::sync::Lazy;
use rand::{
    distributions::Alphanumeric,
    rngs::StdRng,
    Rng,
    SeedableRng,
};
use tokio::sync::{
    Mutex,
    RwLock,
};

use super::{
    encryption,
    ConfigStorageArc,
};
use crate::utils::errors::Error;

/// The cache of all tokens and the username they belong to that are currently
/// in use.
static TOKENS: Lazy<RwLock<HashMap<String, String>>> = Lazy::new(|| RwLock::new(HashMap::new()));
/// The [`StdRng`] that gets used for generating the tokens
static RNG: Lazy<Mutex<StdRng>> = Lazy::new(|| Mutex::new(StdRng::from_entropy()));

/// Generates a token and adds it to the cache together with the corresponding
/// username.
async fn generate_token(username: &str) -> String {
    let token: String = {
        let mut rng = RNG
            .lock()
            .await;
        std::iter::repeat(())
            .map(|_| rng.sample(Alphanumeric))
            .map(char::from)
            .take(32)
            .collect()
    };

    TOKENS
        .write()
        .await
        .insert(token.clone(), username.to_owned());

    token
}

/// Verifies an username-password combination and creates a token for it
pub async fn create_token(
    username: &str,
    password: &str,
    cfg: &ConfigStorageArc,
) -> Result<String, Error> {
    let user = cfg
        .get_user(username)
        .await
        .ok_or(Error::Unauthorized)?;

    if encryption::verify(username, password, &user.password, cfg).await {
        Ok(generate_token(username).await)
    } else {
        Err(Error::Unauthorized)
    }
}

/// Verifies a token exists and returns the corresponsing username if it does
pub async fn verify_token(token: &str) -> Option<String> {
    let token = match token.strip_prefix("Bearer ") {
        Some(t) => t,
        None => return None,
    };

    TOKENS
        .read()
        .await
        .get(token)
        .cloned()
}

#[cfg(test)]
mod tests {
    #[tokio::test]
    async fn token() {
        let token = super::generate_token("test_user").await;

        assert_eq!(
            "test_user",
            super::verify_token(&("Bearer ".to_owned() + &token))
                .await
                .expect("token not found")
        );
    }
}
