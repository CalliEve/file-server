mod encryption;
mod token;

pub use encryption::encrypt;
use hyper::{
    header::AUTHORIZATION,
    Body,
    Request,
};
pub use token::create_token;

use super::{
    config::ConfigStorageArc,
    utils::errors::Error,
};

/// Gets the [`UserConfig`] belonging to the current logged in user, else
/// returns an [`Error::NoToken`] or [`Error::NotAllowed`].
pub async fn get_current_username(req: &Request<Body>) -> Result<String, Error> {
    let given_token = req
        .headers()
        .get(AUTHORIZATION)
        .ok_or(Error::NoToken)?;

    token::verify_token(
        given_token
            .to_str()
            .map_err(|_| Error::NoToken)?,
    )
    .await
    .ok_or(Error::NotAllowed)
}

/// Verifies the user belonging to the token has access to the uri of the
/// request.
pub async fn has_access(req: &Request<Body>, cfg: &ConfigStorageArc) -> Result<(), Error> {
    let user = cfg
        .get_user(&get_current_username(req).await?)
        .await
        .ok_or(Error::NotAllowed)?;

    let path = req
        .uri()
        .path();
    if user
        .access
        .iter()
        .any(|p| p.matches(path))
    {
        Ok(())
    } else {
        Err(Error::NotAllowed)
    }
}

/// Enforces that the current user has access to the given path, else returns an
/// [`Error::NotAllowed`].
pub async fn has_access_to_path<T: AsRef<str>>(
    req: &Request<Body>,
    path: T,
    cfg: &ConfigStorageArc,
) -> Result<(), Error> {
    let user = cfg
        .get_user(&get_current_username(req).await?)
        .await
        .ok_or(Error::NotAllowed)?;

    if user
        .access
        .iter()
        .any(|p| p.matches(path.as_ref()))
    {
        Ok(())
    } else {
        Err(Error::NotAllowed)
    }
}

/// Enforces that the current user is an admin, else returns an
/// [`Error::NotAllowed`].
pub async fn is_admin(req: &Request<Body>, cfg: &ConfigStorageArc) -> Result<(), Error> {
    let user = get_current_username(req).await?;

    if cfg
        .is_admin(&user)
        .await
    {
        Ok(())
    } else {
        Err(Error::NotAllowed)
    }
}
