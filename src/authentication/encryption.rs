use std::num::NonZeroU32;

use once_cell::sync::Lazy;
use ring::{
    digest,
    pbkdf2,
};

use crate::config::ConfigStorageArc;

static ITERATIONS: Lazy<NonZeroU32> = Lazy::new(|| NonZeroU32::new(10_000).unwrap());
static ALG: pbkdf2::Algorithm = pbkdf2::PBKDF2_HMAC_SHA512;
const CRED_LEN: usize = digest::SHA512_OUTPUT_LEN;

/// The type of a created password encryption.
pub type Credential = [u8; CRED_LEN];

/// Creates the encrypted password based on the username and password.
pub async fn encrypt(username: &str, password: &str, cfg: &ConfigStorageArc) -> Credential {
    let salt = salt(username, cfg).await;
    let mut cred = [0_u8; CRED_LEN];
    pbkdf2::derive(ALG, *ITERATIONS, &salt, password.as_bytes(), &mut cred);
    cred
}

/// Verifies the password and cred matches.
pub async fn verify(username: &str, password: &str, cred: &[u8], cfg: &ConfigStorageArc) -> bool {
    // cred would be invalid anyway
    if cred.len() != CRED_LEN {
        return false;
    }

    let salt = salt(username, cfg).await;
    pbkdf2::verify(ALG, *ITERATIONS, &salt, password.as_bytes(), cred).is_ok()
}

/// creates the salt from the username and config.
async fn salt(username: &str, cfg: &ConfigStorageArc) -> Vec<u8> {
    let base = cfg
        .get_salt()
        .await;
    let mut salt = Vec::with_capacity(username.len() + base.len());
    salt.extend(base.as_ref());
    salt.extend(username.as_bytes());
    salt
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use crate::config::{
        config_file::ConfigFile,
        ConfigStorageArc,
    };

    #[tokio::test]
    async fn encryption() {
        let cfg = Arc::new(
            ConfigFile::new()
                .await
                .unwrap(),
        ) as ConfigStorageArc;

        let encrypted = super::encrypt("test_user", "test_pw", &cfg).await;
        assert!(super::verify("test_user", "test_pw", &encrypted, &cfg).await);
        assert!(!super::verify("test_user", "wrong_pw", &encrypted, &cfg).await);
    }
}
