#[allow(unused_extern_crates)]
extern crate proc_macro;

use proc_macro::TokenStream;
use proc_macro2::{
    Ident,
    Span,
};
use quote::quote;
use structs::EndpointFunc;
use syn::parse_macro_input;
use utils::{
    add_suffix,
    CommaSeparatedItems,
    EndpointAttributes,
};

mod structs;
mod utils;

#[proc_macro_attribute]
pub fn endpoint(attr: TokenStream, item: TokenStream) -> TokenStream {
    let endpoint_func = parse_macro_input!(item as EndpointFunc);

    let args = parse_macro_input!(attr as EndpointAttributes);

    let mut use_rest = false;

    for arg in args.args {
        match arg
            .name
            .as_str()
        {
            "use_rest" => {
                use_rest = arg
                    .value
                    .parse()
                    .expect("not a boolean given for use_rest")
            },
            _ => (),
        }
    }

    let func_name = endpoint_func
        .name
        .clone();
    let endpoint_name = add_suffix(&func_name, "ENDPOINT");

    let endpoint_cooked = endpoint_func
        .cooked
        .clone();

    let endpoint_struct_path = quote!(crate::Endpoint);

    let path = args.path;
    let method = Ident::new(
        &args
            .method
            .to_string(),
        Span::call_site(),
    );

    (quote! {
        #(#endpoint_cooked)*
        pub static #endpoint_name: #endpoint_struct_path = #endpoint_struct_path {
            method: ::hyper::Method::#method,
            path: #path,
            use_rest: #use_rest,
            func: #func_name,
        };

        #endpoint_func
    })
    .into()
}

#[proc_macro]
pub fn routes(input: TokenStream) -> TokenStream {
    let named_routes: Vec<Ident> = parse_macro_input!(input as CommaSeparatedItems<Ident>)
        .0
        .into_iter()
        .map(|e| Ident::new(&format!("{}_ENDPOINT", e), Span::call_site()))
        .collect();

    (quote! {
        vec![#(#named_routes.clone()),*]
    })
    .into()
}
