use proc_macro2::{
    Span,
    TokenStream as TokenStream2,
};
use quote::{
    quote,
    ToTokens,
};
use syn::parse::{
    Parse,
    ParseStream,
    Result,
};
use syn::{
    braced,
    Attribute,
    Block,
    FnArg,
    GenericParam,
    Ident,
    Lifetime,
    LifetimeDef,
    PatType,
    ReturnType,
    Signature,
    Stmt,
    Token,
    Type,
    Visibility,
};

pub struct EndpointFunc {
    pub attributes: Vec<Attribute>,
    pub cooked: Vec<Attribute>,
    pub name: Ident,
    pub visibility: Visibility,
    pub sig: Signature,
    pub body: Vec<Stmt>,
}

impl Parse for EndpointFunc {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let attributes = input.call(Attribute::parse_outer)?;

        let (cooked, attributes): (Vec<_>, Vec<_>) = attributes
            .into_iter()
            .partition(|a| {
                a.path
                    .is_ident("cfg")
            });

        let visibility = input.parse::<Visibility>()?;

        let mut sig = input.parse::<Signature>()?;

        match sig.output {
            ReturnType::Type(_, _) => return Err(input.error("expected a default return value")),
            ReturnType::Default => (),
        };

        let body_content;
        braced!(body_content in input);
        let body: Vec<Stmt> = body_content.call(Block::parse_within)?;

        sig = add_lifetimes(sig);

        sig.output = ReturnType::Type(
            Token![->](Span::call_site()),
            Box::new(Type::Verbatim(quote!(
                ::std::pin::Pin<
                    ::std::boxed::Box<
                        (dyn ::std::future::Future<Output = ()> + ::std::marker::Send + 'endpoint),
                    >,
                >
            ))),
        );

        sig.asyncness = None;

        Ok(Self {
            attributes,
            cooked,
            name: sig
                .ident
                .clone(),
            visibility,
            sig,
            body,
        })
    }
}

impl ToTokens for EndpointFunc {
    fn to_tokens(&self, stream: &mut TokenStream2) {
        let Self {
            cooked,
            sig,
            body,
            visibility,
            ..
        } = self;

        stream.extend(quote! {
            #(#cooked)*
            #visibility #sig {
                ::std::boxed::Box::pin(async move {
                    #(#body)*
            })
            }
        });
    }
}

fn add_lifetimes(mut sig: Signature) -> Signature {
    sig.generics
        .params
        .push(GenericParam::Lifetime(LifetimeDef::new(Lifetime::new(
            "'endpoint",
            Span::call_site(),
        ))));

    sig.inputs = sig
        .inputs
        .into_iter()
        .map(|i| match i {
            FnArg::Receiver(_) => panic!("endpoint func can't have a self parameter"),
            FnArg::Typed(arg) => FnArg::Typed(add_arg_lifetime(arg)),
        })
        .collect();
    sig
}

fn add_arg_lifetime(mut arg: PatType) -> PatType {
    match *arg.ty {
        Type::Reference(mut reference) => {
            reference.lifetime = Some(Lifetime::new("'endpoint", Span::call_site()));
            arg.ty = Box::new(Type::Reference(reference));
            arg
        },
        _ => arg,
    }
}
