use hyper::Method;
use proc_macro2::{
    Ident,
    Literal
};
use quote::format_ident;
use syn::{
    parenthesized,
    parse::{
        Parse,
        ParseStream,
        Result,
    },
    punctuated::Punctuated,
    token::Comma,
    Token,
    Lit,
};

pub struct ParenthesisedItems<T>(pub Punctuated<T, Comma>);

impl<T: Parse> Parse for ParenthesisedItems<T> {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let content;
        parenthesized!(content in input);
        Ok(Self(content.parse_terminated(T::parse)?))
    }
}

pub struct CommaSeparatedItems<T>(pub Punctuated<T, Comma>);

impl<T: Parse> Parse for CommaSeparatedItems<T> {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        Ok(Self(input.parse_terminated(T::parse)?))
    }
}

pub fn add_suffix(ident: &Ident, suffix: &str) -> Ident {
    format_ident!("{}_{}", ident.to_string(), suffix)
}

pub struct EndpointAttributes {
    pub method: Method,
    pub path: String,
    pub args: Punctuated<NamedArgs, Comma>,
}

impl Parse for EndpointAttributes {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let method_ident = input.parse::<Ident>()?;
        let method = if let Ok(m) = method_ident
            .to_string()
            .parse::<Method>()
        {
            m
        } else {
            return Err(syn::Error::new(
                method_ident.span(),
                "Not a valid http Method",
            ));
        };
        let path = input
            .parse::<Literal>()?
            .to_string()
            .trim_matches('\"')
            .to_owned();

        Ok(if let Ok(_) = input.parse::<Token![,]>() {
            Self {
                method,
                path,
                args: input.parse_terminated(NamedArgs::parse)?,
            }
        } else {
            Self {
                method,
                path,
                args: Punctuated::new(),
            }
        })
    }
}

pub struct NamedArgs {
    pub name: String,
    pub value: String,
}

impl Parse for NamedArgs {
    fn parse(input: ParseStream<'_>) -> Result<Self> {
        let name = input
            .parse::<Ident>()?
            .to_string();
        input.parse::<Token![=]>()?;
        let mut value = match input
            .parse::<Lit>()? {
                Lit::Bool(b) => b.value.to_string(),
                Lit::Str(s) => s.value(),
                x => panic!("Invalid argument: {:?}", x),
            };
        value = value
            .trim_start_matches('\"')
            .to_owned();
        value = value
            .trim_end_matches('\"')
            .to_owned();

        Ok(Self {
            name,
            value,
        })
    }
}
